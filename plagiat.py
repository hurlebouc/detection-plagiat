# -*- coding: utf-8 -*-
from ast import *

p1 = parse(open("test.py").read())
p2 = parse(open("test2.py").read())

# Améliorations possibles :
#   - rendre le 3e parametre inutile en faisant de l'alpha-renommage
#   - rassembler les groupes de code "normaux" entre eux
#   - Donner la possibilité de donner un masque (pour empècher la comparaison
#     de certains bouts de code : par exemple lorsque les élèves partent d'un 
#     modèle
#   - faire une fonction de parcours générique (genre fold_left)

#('a -> 'b -> 'a) -> 'a -> 'b iterable -> 'a
def fold_left(f, a, l): 
    res = a
    for b in l:
        res = f(res, b)
    return res

#('a -> 'b -> 'c -> 'a) -> 'a -> 'b ordening -> 'c ordening -> 'a
def fold_left2(f, a, l1, l2):
    res = a
    for i in range(min(len(l1), len(l2))):
        res = f(res, l1[i], l2[i])
    return res

def equalList(lp1, lp2, nameSensitive):
    return fold_left2(
            (lambda b, p1, p2: b and equal(p1, p2, nameSensitive)),
            True,
            lp1,
            lp2)

def equalIdList(lid1, lid2, nameSensitive):
    return fold_left2(
            (lambda b, id1, id2: b and equalId(id1, id2, nameSensitive)),
            True,
            lid1,
            lid2)

#class Identifier:
#    def __init(self, val):
#        self.identificator = val
#
#class Flat:
#    def __init(self, val):
#        self.flat = val
#
#class Leaves:
#    def __init(self, val):
#        self.flat = val

def subAST(ast):
    case = type(ast).__name__
    if case in ("Module", "Interactive", "Suite"):
        return (ast.body,)
    if case == "Expression":
        return (ast.body,)
    if case == "FunctionDef":
        #return (Identifier(ast.name), ast.args, ast.body, ast.decorator_list)
        return (ast.args, ast.body, ast.decorator_list)
    if case == "ClassDef":
        #return (Identifier(ast.name), ast.bases, ast.body, ast.decorator_list)
        return (ast.bases, ast.body, ast.decorator_list)
    if case == "Return":
        return (ast.value,)
    if case == "Delete":
        return (ast.targets)
    if case == "Assign":
        return (ast.targets, ast.value)
    if case == "AugAssign":
        return (ast.op, ast.target, ast.value)
    if case == "Print":
        #return (ast.dest, ast.values, Flat(ast.nl))
        return (ast.dest, ast.values)
    if case == "For":
        return (ast.target, ast.iter, ast.body, ast.orelse)
    if case == "While":
        return (ast.test, ast.body, ast.orelse)
    if case == "If":
        return (ast.test, ast.body, ast.orelse)
    if case == "With":
        return (ast.context_expr, ast.optional_vars, ast.body)
    if case == "Raise":
        return (ast.type, ast.inst, ast.tback)
    if case == "TryExcept":
        return (ast.body, ast.handlers, ast.orelse)
    if case == "TryFinally":
        return (ast.body, ast.finalbody)
    if case == "Assert":
        return (ast.test, ast.msg)
    if case == "Import":
        return (ast.names,)
    if case == "ImportFrom":
        #return (Identifier(ast.module), ast.names, Flat(ast.level))
        return (ast.names,)
    if case == "Exec":
        return (ast.body, ast.globals, ast.locals)
    if case == "Global":
        return (ast.names,)
    if case == "Expr":
        return (ast.value,)
    if case == "attributes":
        #return (Flat(ast.lineno), Flat(ast.col_offset))
        return ()
    if case == "BoolOp":
        return (ast.op, ast.values)
    if case == "BinOp":
        return (ast.op, ast.left, ast.right)
    if case == "UnaryOp":
        return (ast.op, ast.operand)
    if case == "Lambda":
        return (ast.args, ast.body)
    if case == "IfExp":
        return (ast.test, ast.body, ast.orelse)
    if case == "Dict":
        return (ast.keys, ast.values)
    if case == "Set":
        return (ast.elts,)
    if case == "ListComp":
        return (ast.elt, ast.generators)
    if case == "SetComp":
        return (ast.elt, ast.generators)
    if case == "DictComp":
        return (ast.key, ast.value, ast.generators)
    if case == "GeneratorExp":
        return (ast.elt, ast.generators)
    if case == "Yield":
        return (ast.value,)
    if case == "Compare":
        return (ast.ops, ast.left, ast.comparators)
    if case == "Call":
        return (ast.func, ast.starargs, ast.kwargs, ast.keywords, ast.args)
    if case == "Repr":
        return (ast.value,)
    if case == "Num":
        #return (Flat(ast.n),)
        return ()
    if case == "Str":
        #return (Flat(ast.s),)
        return ()
    if case == "Attribute":
        #return (ast.value, ast.ctx, Identifier(ast.attr))
        return (ast.value, ast.ctx)
    if case == "Subscript":
        return (ast.value, ast.slice, ast.ctx)
    if case == "Name":
        #return (Identifier(ast.id), ast.ctx)
        return (ast.ctx,)
    if case == "List":
        return (ast.ctx, ast.elts)
    if case == "Tuple":
        return (ast.ctx, ast.elts)
    if case == "Slice":
        return (ast.lower, ast.upper, ast.step)
    if case == "ExtSlice":
        return (ast.dims,)
    if case == "Index":
        return (ast.value,)
    if case == "comprehension":
        return (ast.target, ast.iter, ast.ifs)
    if case == "excepthandler":
        return (ast.type, ast.name, ast.body)
    if case == "arguments":
        #return (Identifier(ast.vararg),Identifier(ast.kwarg),ast.defaults,ast.args)
        return (ast.defaults, ast.args)
    if case == "keyword":
        #return (ast.value, Identifier(ast.arg))
        return (ast.value,)
    if case == "alias":
        #return (Identifier(ast.name), Identifier(ast.asname))
        return ()
    if case in ("Pass", "Break", "Continue", "Load", "Store", "Del", "AugLoad", "AugStore", "Param", "Ellipsis", "And", "Or", "Add", "Sub", "Mult", "Div", "Mod", "Pow", "LShift", "RShift", "BitOr", "BitXor", "BitAnd", "FloorDiv", "Invert", "Not", "UAdd", "USub", "Eq", "NotEq", "Lt", "LtE", "Gt", "GtE", "Is", "IsNot", "In", "NotIn", ):
        return ()
    erreur = "format inconnu : %s" % case
    raise NameError(erreur)


# ('a -> node -> 'a) -> a -> node ast -> 'a
def foldAST(f, a, ast):
    a = f(a, ast)
    sAST = subAST(ast)
    for e in sAST:
        if type(e).__name in ("list", "tuple"):
            for ast2 in e:
                a = foldAST(f, a, ast2)
        else:
            a = foldAST(f, a, e)
    return a

# pas très utile ?!
#def foldAST2(f, a, ast1, ast2):
#    a = f(a, ast1, ast2)
#    sAST1 = subAST(ast1)
#    sAST2 = subAST(ast2)
#    l1 == len(sAST1)
#    l2 == len(sAST2)
#    for i in range(min(l1, l2)):
#        if type(sAST1[i]).__name__ in ("list", "tuple"):
#            ll1 = len(sAST1[i])
#            ll2 = len(sAST2[i])
#            for j in range(min(ll1, ll2)):
#                a = foldAST2(f, a, sAST1[i][j], sAST2[i][j])
#        else:
#            a = foldAST2(f, a, sAST1[i], sAST2[i])
#    return a

def equal(ast1, ast2, nameSensitive=True):

    def equalId(id1, id2):
        return not nameSensitive or id1 == id2

    def f(ast1, ast2):
        if type(ast1).__name__ != type(ast2).__name__:
            return False
        case = type(ast1).__name__
        if case in ("FunctionDef", "ClassDef"):
            return equalId(ast1.name, ast2.name)
        if case == "ImportFrom":
            return equalId(ast1.module, ast2.module) and ast1.level == ast2.level
        if case == "Attribute":
            return equalId(ast1.attr, ast2.attr)
        if case == "Name":
            return equalId(ast1.id, ast2.id)
        if case == "arguments":
            return equalId(ast1.vararg, ast2.vararg) and equalId(ast1.kwarg, ast2.kwarg)
        if case == "keyword":
            return equalId(ast1.arg, ast2.arg)
        if case == "alias":
            return equalId(ast1.name,ast2.name) and equalId(ast1.asname, ast2.asname)
        if case == "Print":
            return ast1.nl == ast2.nl
        if case == "attributes":
            return ast1.lineno == ast2.lineno and ast1.col_offset == ast2.col_offset
        if case == "Num":
            return ast1.n == ast2.n
        if case == "Str":
            return ast1.s == ast2.s
        return True

    def iter(ast1, ast2):
        if ast1 == None and ast2 == None:
            return True
        if not f(ast1, ast2):
            return False
        sAST1 = subAST(ast1)
        sAST2 = subAST(ast2)
        l1 = len(sAST1)
        #l2 = len(sAST2) que l'on sait être égal à l1, grâce à f(...) == True
        for i in range(l1):
            if type(sAST1[i]).__name__ in ("list", "tuple"):
                ll1 = len(sAST1[i])
                ll2 = len(sAST2[i])
                if ll1 != ll2:
                    return False
                for j in range(ll1):
                    if not iter(sAST1[i][j], sAST2[i][j]):
                        return False
            else:
                if not iter(sAST1[i], sAST2[i]):
                    return False
        return True

    return iter(ast1, ast2)


#def equal(p1, p2, nameSensitive=True):
#    if type(p1).__name__ != type(p2).__name__:
#        return False
#    case = type(p1).__name__
#    if case == "NoneType":
#        return True
##    if case == "list":
##        return equalList(p1, p2, nameSensitive)
#    if case in ("Module", "Interactive", "Suite"):
#        return equalList(p1.body, p2.body, nameSensitive)
#    if case == "Expression":
#        return equal(p1.body, p2.body, nameSensitive)
#    if case == "FunctionDef":
#        return (equalId(p1.name, p2.name, nameSensitive) 
#            and equal(p1.args, p2.args, nameSensitive) 
#            and equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.decorator_list, p2.decorator_list, nameSensitive)
#                )
#    if case == "ClassDef":
#        return (equalId(p1.name, p2.name, nameSensitive) 
#            and equalList(p1.bases, p2.bases, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.decorator_list, p2.decorator_list, nameSensitive)
#                )
#    if case == "Return":
#        return equal(p1.value, p2.value, nameSensitive)
#    if case == "Delete":
#        return equalList(p1.targets, p2.targets, nameSensitive)
#    if case == "Assign":
#        return (equalList(p1.targets, p2.targets, nameSensitive)
#            and equal(p1.value, p2.value, nameSensitive))
#    if case == "AugAssign":
#        return (equal(p1.target, p2.target, nameSensitive)
#            and equal(p1.op, p2.op, nameSensitive)
#            and equal(p1.value, p2.value, nameSensitive))
#    if case == "Print":
#        return (equal(p1.dest, p2.dest, nameSensitive)
#            and equalList(p1.values, p2.values, nameSensitive)
#            and p1.nl == p2.nl)
#    if case == "For":
#        return (equal(p1.target, p2.target, nameSensitive)
#            and equal(p1.iter, p2.iter, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.orelse, p2.orelse, nameSensitive))
#    if case == "While":
#        return (equal(p1.test, p2.test, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.orelse, p2.orelse, nameSensitive))
#    if case == "If":
#        return (equal(p1.test, p2.test, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.orelse, p2.orelse, nameSensitive))
#    if case == "With":
#        return (equal(p1.context_expr, p2.context_expr, nameSensitive)
#            and equal(p1.optional_vars, p2.optional_vars, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive))
#    if case == "Raise":
#        return (equal(p1.type, p2.type, nameSensitive)
#            and equal(p1.inst, p2.inst, nameSensitive)
#            and equal(p1.tback, p2.tback, nameSensitive))
#    if case == "TryExcept":
#        return (equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.handlers, p2.handlers, nameSensitive)
#            and equalList(p1.orelse, p2.orelse, nameSensitive))
#    if case == "TryFinally":
#        return (equalList(p1.body, p2.body, nameSensitive)
#            and equalList(p1.finalbody, p2.finalbody, nameSensitive))
#    if case == "Assert":
#        return (equal(p1.test, p2.test, nameSensitive)
#            and equal(p1.msg, p2.msg, nameSensitive))
#    if case == "Import":
#        return equalList(p1.names, p2.names, nameSensitive)
#    if case == "ImportFrom":
#        return (equalId(p1.module, p2.module, nameSensitive) 
#            and equalList(p1.names, p2.names, nameSensitive)
#            and (p1.level == p2.level))
#    if case == "Exec":
#        return (equal(p1.body, p2.body, nameSensitive)
#            and equal(p1.globals, p2.globals, nameSensitive)
#            and equal(p1.locals, p2.locals, nameSensitive))
#    if case == "Global":
#        return equalIdList(p1.names, p2.names, nameSensitive)
#    if case == "Expr":
#        return equal(p1.value, p2.value, nameSensitive)
#    if case == "attributes":
#        return p1.lineno == p2.lineno and p1.col_offset == p2.col_offset
#    if case == "BoolOp":
#        return (equal(p1.op, p2.op, nameSensitive)
#            and equalList(p1.values, p2.values, nameSensitive))
#    if case == "BinOp":
#        return (equal(p1.left, p2.left, nameSensitive)
#            and equal(p1.op, p2.op, nameSensitive)
#            and equal(p1.right, p2.right, nameSensitive))
#    if case == "UnaryOp":
#        return (equal(p1.op, p2.op, nameSensitive)
#            and equal(p1.operand, p2.operand, nameSensitive))
#    if case == "Lambda":
#        return (equal(p1.args, p2.args, nameSensitive)
#            and equal(p1.body, p2.body, nameSensitive))
#    if case == "IfExp":
#        return (equal(p1.test, p2.test, nameSensitive)
#            and equal(p1.body, p2.body, nameSensitive)
#            and equal(p1.orelse, p2.orelse, nameSensitive))
#    if case == "Dict":
#        return (equalList(p1.keys, p2.keys, nameSensitive)
#            and equalList(p1.values, p2.values, nameSensitive))
#    if case == "Set":
#        return equalList(p1.elts, p2.elts, nameSensitive)
#    if case == "ListComp":
#        return (equal(p1.elt, p2.elt, nameSensitive)
#            and equalList(p1.generators, p2.generators, nameSensitive))
#    if case == "SetComp":
#        return (equal(p1.elt, p2.elt, nameSensitive)
#            and equalList(p1.generators, p2.generators, nameSensitive))
#    if case == "DictComp":
#        return (equal(p1.key, p2.key, nameSensitive)
#            and equal(p1.value, p2.value, nameSensitive)
#            and equalList(p1.generators, p2.generators, nameSensitive))
#    if case == "GeneratorExp":
#        return (equal(p1.elt, p2.elt, nameSensitive)
#            and equalList(p1.generators, p2.generators, nameSensitive))
#    if case == "Yield":
#        return equal(p1.value, p2.value, nameSensitive)
#    if case == "Compare":
#        return (equal(p1.left, p2.left, nameSensitive)
#            and equalList(p1.comparators, p2.comparators, nameSensitive)
#            and equalList(p1.ops, p2.ops, nameSensitive))
#    if case == "Call":
#        return (equal(p1.func, p2.func, nameSensitive)
#            and equal(p1.starargs, p2.starargs, nameSensitive)
#            and equal(p1.kwargs, p2.kwargs, nameSensitive)
#            and equalList(p1.keywords, p2.keywords, nameSensitive)
#            and equalList(p1.args, p2.args, nameSensitive))
#    if case == "Repr":
#        return equal(p1.value, p2.value, nameSensitive)
#    if case == "Num":
#        return p1.n == p2.n
#    if case == "Str":
#        return p1.s == p2.s
#    if case == "Attribute":
#        return (equal(p1.value, p2.value, nameSensitive)
#            and equal(p1.ctx, p2.ctx, nameSensitive)
#            and equalId(p1.attr, p2.attr, nameSensitive))
#    if case == "Subscript":
#        return (equal(p1.value, p2.value, nameSensitive)
#            and equal(p1.slice, p2.slice, nameSensitive)
#            and equal(p1.ctx, p2.ctx, nameSensitive))
#    if case == "Name":
#        return (equalId(p1.id, p2.id, nameSensitive)
#            and equal(p1.ctx, p2.ctx, nameSensitive))
#    if case == "List":
#        return (equal(p1.ctx, p2.ctx, nameSensitive)
#            and equalList(p1.elts, p2.elts, nameSensitive))
#    if case == "Tuple":
#        return (equal(p1.ctx, p2.ctx, nameSensitive)
#            and equalList(p1.elts, p2.elts, nameSensitive))
#    if case == "Slice":
#        return (equal(p1.lower, p2.lower, nameSensitive)
#            and equal(p1.upper, p2.upper, nameSensitive)
#            and equal(p1.step, p2.step, nameSensitive))
#    if case == "ExtSlice":
#        return equalList(p1.dims, p2.dims, nameSensitive)
#    if case == "Index":
#        return equal(p1.value, p2.value, nameSensitive)
#    if case == "comprehension":
#        return (equal(p1.target, p2.target, nameSensitive)
#            and equal(p1.iter, p2.iter, nameSensitive)
#            and equalList(p1.ifs, p2.ifs, nameSensitive))
#    if case == "excepthandler":
#        return (equal(p1.type, p2.type, nameSensitive)
#            and equal(p1.name, p2.name, nameSensitive)
#            and equalList(p1.body, p2.body, nameSensitive))
#    if case == "arguments":
#        return (equalId(p1.vararg, p2.vararg, nameSensitive)
#            and equalId(p1.kwarg, p2.kwarg, nameSensitive)
#            and equalList(p1.defaults, p2.defaults, nameSensitive)
#            and equalList(p1.args, p2.args, nameSensitive))
#    if case == "keyword":
#        return (equal(p1.value, p2.value, nameSensitive)
#            and equalId(p1.arg, p2.arg, nameSensitive))
#    if case == "alias":
#        return (equalId(p1.name, p2.name, nameSensitive)
#            and equalId(p1.asname, p2.asname, nameSensitive))
#    if case in ("Pass", "Break", "Continue", "Load", "Store", "Del", "AugLoad", "AugStore", "Param", "Ellipsis", "And", "Or", "Add", "Sub", "Mult", "Div", "Mod", "Pow", "LShift", "RShift", "BitOr", "BitXor", "BitAnd", "FloorDiv", "Invert", "Not", "UAdd", "USub", "Eq", "NotEq", "Lt", "LtE", "Gt", "GtE", "Is", "IsNot", "In", "NotIn", ):
#        return True
#    raise NameError("format inconnu")

print(equal(p1, p2, False))
